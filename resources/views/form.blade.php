<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link
            rel="stylesheet"
            href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
            integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh"
            crossorigin="anonymous">
        <script
            src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
            integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
            crossorigin="anonymous"></script>
        <script
            src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
            integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
            crossorigin="anonymous"></script>
        <script
            src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
            integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
            crossorigin="anonymous"></script>
        <title>Document</title>
    </head>
    <body>

        <nav class="navbar" style="background-color:#013243">
            <a class="navbar-brand" href="#" style="color:white;">Pendaftaran Siswa Online</a>
        </nav>

        <br>
        <br>

        <form
            action="{{ route('form.store') }}"
            method="POST"
            enctype="multipart/form-data">
            @csrf
            <div class="container">
                <div class="card">
                    <div class="card-header">
                        I. DATA CALON MURID
                    </div>

                    <div class="container">
                        <div class="form-group">
                            <br>
                            <label for="exampleFormControlInput1">Foto</label>
                            <input name="foto" type="file" class="form-control">
                        </div>

                        <div class="form-group">
                            <input
                                name="nama_lengkap"
                                type="text"
                                class="form-control"
                                id="exampleFormControlInput1"
                                placeholder="Masukkan nama lengkap anda">
                        </div>

                        <div class="form-group">
                            <input
                                name="nama_panggilan"
                                type="text"
                                class="form-control"
                                id="exampleFormControlInput1"
                                placeholder="Masukkan nama panggilan anda">
                        </div>

                        <div class="form-group">
                            <label for="">Jenis Kelamin</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="col">
                                <input
                                    class="form-check-input"
                                    type="radio"
                                    name="jenis_kelamin"
                                    id="inlineRadio2"
                                    value="Laki-laki">
                                <label class="form-check-label" for="inlineRadio2">Laki-laki</label>
                            </div>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="col">
                                <input
                                    class="form-check-input"
                                    type="radio"
                                    name="jenis_kelamin"
                                    id="inlineRadio3"
                                    value="Perempuan">
                                <label class="form-check-label" for="inlineRadio3">Perempuan</label>
                            </div>
                        </div>

                        <br>
                        <br>

                        <div class="form-group">
                            <label for="">Tempat Tanggal Lahir</label>
                            <div class="form-row">
                                <div class="col">
                                    <input
                                        type="date"
                                        class="form-control"
                                        placeholder="First name"
                                        id="tanggal"
                                        onchange="price()">
                                </div>
                                <div class="col">
                                    <input
                                        type="text"
                                        class="form-control"
                                        placeholder="Nama Kota"
                                        id="kota"
                                        onchange="price()">
                                </div>
                            </div>
                            <div class="form-group">
                                <input
                                    type="text"
                                    class="form-control"
                                    name="ttl"
                                    id="tujuan"
                                    readonly="readonly"
                                    hidden="hidden"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <input
                                name="agama"
                                type="text"
                                class="form-control"
                                id="exampleFormControlInput1"
                                placeholder="Agama">
                        </div>

                        <div class="form-group">
                            <input
                                name="cita_cita"
                                type="text"
                                class="form-control"
                                id="exampleFormControlInput1"
                                placeholder="Cita-cita">
                        </div>

                        <div class="form-group">
                            <input
                                name="hobi"
                                type="text"
                                class="form-control"
                                id="exampleFormControlInput1"
                                placeholder="Hobi">
                        </div>

                        <div class="form-group">
                            <input
                                name="anak_ke"
                                type="text"
                                class="form-control"
                                id="exampleFormControlInput1"
                                placeholder="Anak ke">
                        </div>

                        <div class="form-group">
                            <label for="">Jumlah Saudara</label>
                            <div class="form-row">
                                <div class="col">
                                    <input name="kandung" type="number" class="form-control" placeholder="Kandung">
                                </div>

                                <div class="col">
                                    <input name="tiri" type="number" class="form-control" placeholder="Tiri">
                                </div>

                                <div class="col">
                                    <input name="angkat" type="number" class="form-control" placeholder="Angkat">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-row">
                                <div class="col">
                                    <input
                                        name="berat_badan"
                                        type="number"
                                        class="form-control"
                                        placeholder="Berat Badan (kg)">
                                </div>

                                <div class="col">
                                    <input
                                        name="tinggi_badan"
                                        type="number"
                                        class="form-control"
                                        placeholder="Tinggi Badan (cm)">
                                </div>

                                <div class="col">
                                    <input
                                        name="gol_darah"
                                        type="text"
                                        class="form-control"
                                        placeholder="Golongan Darah">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <br>
                <br>

                <div class="card">
                    <div class="card-header">
                        II. KETERANGAN TEMPAT TINGGAL
                    </div>
                    <br>
                    <div class="container">
                        <div class="form-group">
                            <div class="form-row">
                                <div class="col-8">
                                    <label for="">Alamat Rumah</label>
                                    <input
                                        name="alamat_rumah"
                                        type="text"
                                        class="form-control"
                                        placeholder="Alamat Rumah">
                                </div>

                                <div class="col">
                                    <label for="" style="opacity:0;">RW</label>
                                    <input name="rt" type="number" class="form-control" placeholder="RT">
                                </div>

                                <div class="col">
                                    <label for="" style="opacity:0;">RW</label>
                                    <input name="rw" type="number" class="form-control" placeholder="RW">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-row">
                                <div class="col">
                                    <input
                                        name="kelurahan"
                                        type="text"
                                        class="form-control"
                                        placeholder="Kelurahan">
                                </div>

                                <div class="col">
                                    <input
                                        name="kecamatan"
                                        type="text"
                                        class="form-control"
                                        placeholder="Kecamatan">
                                </div>

                                <div class="col">
                                    <input
                                        name="kota_kabupaten"
                                        type="text"
                                        class="form-control"
                                        placeholder="Kota / Kabupaten">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-row">
                                <div class="col">
                                    <input name="provinsi" type="text" class="form-control" placeholder="Provinsi">
                                </div>

                                <div class="col">
                                    <input name="kode_pos" type="text" class="form-control" placeholder="Kode Pos">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-row">
                                <div class="col">
                                    <input
                                        name="no_telp_rumah"
                                        type="text"
                                        class="form-control"
                                        placeholder="No. Telp Rumah">
                                </div>

                                <div class="col">
                                    <input name="email" type="text" class="form-control" placeholder="Email">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="">Tinggal Dengan</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="col">
                                <input
                                    class="form-check-input"
                                    type="radio"
                                    name="tinggal_dengan"
                                    id="inlineRadio2"
                                    value="Orang Tua">
                                <label class="form-check-label" for="inlineRadio2">Orang Tua</label>
                            </div>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="col">
                                <input
                                    class="form-check-input"
                                    type="radio"
                                    name="tinggal_dengan"
                                    id="inlineRadio3"
                                    value="Asrama">
                                <label class="form-check-label" for="inlineRadio3">Asrama</label>
                            </div>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="col">
                                <input
                                    class="form-check-input"
                                    type="radio"
                                    name="tinggal_dengan"
                                    id="inlineRadio4"
                                    value="Asrama">
                                <label class="form-check-label" for="inlineRadio4">Saudara</label>
                            </div>
                        </div>
                        <br>
                        <br>
                    </div>
                </div>

                <br>
                <br>

                <div class="card">
                    <div class="card-header">
                        III. KETERANGAN KESEHATAN
                    </div>
                    <br>
                    <div class="container">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Penyakit yang pernah diderita</label>
                            <input
                                name="penyakit_diderita"
                                type="text"
                                class="form-control"
                                id="exampleFormControlInput1"
                                placeholder="Penyakit yang pernah diderita">
                        </div>
                        <div class="form-group">
                            <label for="exampleFormControlSelect1">Kelainan Jasmani / Lainnya</label>
                            <input
                                name="kelainan_lain"
                                type="text"
                                class="form-control"
                                id="exampleFormControlInput1"
                                placeholder="Kelainan Jasmani / Lainnya">
                        </div>
                        <small>*Kosongkan jika tidak ada</small>
                    </div>
                </div>

                <br>
                <br>

                <div class="card">
                    <div class="card-header">
                        IV. DATA ORANG TUA / WALI
                    </div>
                    <br>
                    <div class="container">
                        <label for="">A. Ayah</label>
                        <div class="form-group">
                            <input
                                name="nama_ayah"
                                type="text"
                                class="form-control"
                                id="exampleFormControlInput1"
                                placeholder="Nama Ayah">
                        </div>

                        <div class="form-group">
                            <label for="">Keberadaan</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="col">
                                <input
                                    class="form-check-input"
                                    type="radio"
                                    name="keberadaan_ayah"
                                    id="inlineRadio2"
                                    value="Masih Ada">
                                <label class="form-check-label" for="inlineRadio2">Masih Ada</label>
                            </div>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="col">
                                <input
                                    class="form-check-input"
                                    type="radio"
                                    name="keberadaan_ayah"
                                    id="inlineRadio3"
                                    value="Almarhum">
                                <label class="form-check-label" for="inlineRadio3">Almarhum</label>
                            </div>
                        </div>

                        <br>
                        <br>

                        <div class="form-group">
                            <label for="">Tempat Tanggal Lahir</label>
                            <div class="form-row">
                                <div class="col">
                                    <input
                                        type="date"
                                        class="form-control"
                                        placeholder="First name"
                                        id="tanggal_ayah"
                                        onchange="price()">
                                </div>
                                <div class="col">
                                    <input
                                        type="text"
                                        class="form-control"
                                        placeholder="Nama Kota"
                                        id="kota_ayah"
                                        onchange="price()">
                                </div>
                            </div>
                            <div class="form-group">
                                <input
                                    type="text"
                                    class="form-control"
                                    name="ttl_ayah"
                                    id="tujuan_ayah"
                                    readonly="readonly"
                                    hidden="hidden"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-row">
                                <div class="col">
                                    <input
                                        name="pekerjaan_ayah"
                                        type="text"
                                        class="form-control"
                                        placeholder="Pekerjaan">
                                </div>

                                <div class="col">
                                    <input
                                        name="pendidikan_ayah"
                                        type="text"
                                        class="form-control"
                                        placeholder="Pendidikan Terakhir">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-row">
                                <div class="col">
                                    <input
                                        name="wn_ayah"
                                        type="text"
                                        class="form-control"
                                        placeholder="Kewarganegaraan">
                                </div>

                                <div class="col">
                                    <input name="agama_ayah" type="text" class="form-control" placeholder="Agama">
                                </div>

                                <div class="col">
                                    <input name="telp_ayah" type="text" class="form-control" placeholder="No. HP">
                                </div>
                            </div>
                        </div>
                        <hr>
                        <label for="">B. Ibu</label>
                        <div class="form-group">
                            <input
                                name="nama_ibu"
                                type="text"
                                class="form-control"
                                id="exampleFormControlInput1"
                                placeholder="Nama Ibu">
                        </div>

                        <div class="form-group">
                            <label for="">Keberadaan</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="col">
                                <input
                                    class="form-check-input"
                                    type="radio"
                                    name="keberadaan_ibu"
                                    id="inlineRadio2"
                                    value="Masih Ada">
                                <label class="form-check-label" for="inlineRadio2">Masih Ada</label>
                            </div>
                        </div>
                        <div class="form-check form-check-inline">
                            <div class="col">
                                <input
                                    class="form-check-input"
                                    type="radio"
                                    name="keberadaan_ibu"
                                    id="inlineRadio3"
                                    value="Almarhum">
                                <label class="form-check-label" for="inlineRadio3">Almarhum</label>
                            </div>
                        </div>

                        <br>
                        <br>

                        <div class="form-group">
                            <label for="">Tempat Tanggal Lahir</label>
                            <div class="form-row">
                                <div class="col">
                                    <input
                                        type="date"
                                        class="form-control"
                                        placeholder="First name"
                                        id="tanggal_ibu"
                                        onchange="price()">
                                </div>
                                <div class="col">
                                    <input
                                        type="text"
                                        class="form-control"
                                        placeholder="Nama Kota"
                                        id="kota_ibu"
                                        onchange="price()">
                                </div>
                            </div>
                            <div class="form-group">
                                <input
                                    type="text"
                                    class="form-control"
                                    name="ttl_ibu"
                                    id="tujuan_ibu"
                                    readonly="readonly"
                                    hidden="hidden"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-row">
                                <div class="col">
                                    <input
                                        name="pekerjaan_ibu"
                                        type="text"
                                        class="form-control"
                                        placeholder="Pekerjaan">
                                </div>

                                <div class="col">
                                    <input
                                        name="pendidikan_ibu"
                                        type="text"
                                        class="form-control"
                                        placeholder="Pendidikan Terakhir">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-row">
                                <div class="col">
                                    <input
                                        name="wn_ibu"
                                        type="text"
                                        class="form-control"
                                        placeholder="Kewarganegaraan">
                                </div>

                                <div class="col">
                                    <input name="agama_ibu" type="text" class="form-control" placeholder="Agama">
                                </div>

                                <div class="col">
                                    <input name="telp_ibu" type="text" class="form-control" placeholder="No. HP">
                                </div>
                            </div>
                        </div>

                        <hr>
                        <label for="">C. Wali</label>
                        <div class="form-group">
                            <input
                                name="nama_wali"
                                type="text"
                                class="form-control"
                                id="exampleFormControlInput1"
                                placeholder="Nama Wali">
                        </div>

                        <div class="form-group">
                            <label for="">Tempat Tanggal Lahir</label>
                            <div class="form-row">
                                <div class="col">
                                    <input
                                        type="date"
                                        class="form-control"
                                        placeholder="First name"
                                        id="tanggal_wali"
                                        onchange="price()">
                                </div>
                                <div class="col">
                                    <input
                                        type="text"
                                        class="form-control"
                                        placeholder="Nama Kota"
                                        id="kota_wali"
                                        onchange="price()">
                                </div>
                            </div>
                            <div class="form-group">
                                <input
                                    type="text"
                                    class="form-control"
                                    name="ttl_wali"
                                    id="tujuan_wali"
                                    readonly="readonly"
                                    hidden="hidden"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-row">
                                <div class="col">
                                    <input
                                        name="pekerjaan_wali"
                                        type="text"
                                        class="form-control"
                                        placeholder="Pekerjaan">
                                </div>

                                <div class="col">
                                    <input
                                        name="pendidikan_wali"
                                        type="text"
                                        class="form-control"
                                        placeholder="Pendidikan Terakhir">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-row">
                                <div class="col">
                                    <input
                                        name="wn_wali"
                                        type="text"
                                        class="form-control"
                                        placeholder="Kewarganegaraan">
                                </div>

                                <div class="col">
                                    <input name="agama_wali" type="text" class="form-control" placeholder="Agama">
                                </div>

                                <div class="col">
                                    <input name="telp_wali" type="text" class="form-control" placeholder="No. HP">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="form-row">
                                <div class="col">
                                    <input
                                        name="hubungan_wali"
                                        type="text"
                                        class="form-control"
                                        placeholder="Hubungan dengan murid">
                                </div>

                                <div class="col">
                                    <input name="email_wali" type="text" class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <br>
                            <small>*Kosongkan jika tidak ada</small>
                        </div>
                    </div>
                </div>

                <br>
                <br>

                <div class="card">
                    <div class="card-header">
                        V. NILAI RAPOR
                    </div>
                    <br>
                    <div class="container">
                        <div class="table-responsive">
                            <table class="table table-bordered">
                                <thead align="center">
                                    <tr>
                                        <th rowspan="2">No</th>
                                        <th colspan="2" rowspan="2">Mata Pelajaran</th>
                                        <th colspan="2">Kelas 7</th>
                                        <th colspan="2">Kelas 8</th>
                                        <th scope="col">Kelas 9</th>
                                    </tr>
                                    <tr>
                                        <th>Semester 1</th>
                                        <th>Semester 2</th>
                                        <th>Semester 1</th>
                                        <th>Semester 2</th>
                                        <th>Semester 1</th>
                                    </tr>
                                </thead>
                                <tbody align="center">
                                    <tr>
                                        <th scope="row">1</th>
                                        <td colspan="2" align="left">PAI</td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="pai_satu"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="pai_dua"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="pai_tiga"></td>
                                        <td><input
                                            type="text"
                                            style="border:0;text-align:center;"
                                            size="6"
                                            name="pai_empat"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="pai_lima"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">2</th>
                                        <td colspan="2" align="left">Bahasa Indonesia</td>
                                        <td><input
                                            type="text"
                                            style="border:0;text-align:center;"
                                            size="6"
                                            name="indo_satu"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="indo_dua"></td>
                                        <td><input
                                            type="text"
                                            style="border:0;text-align:center;"
                                            size="6"
                                            name="indo_tiga"></td>
                                        <td><input
                                            type="text"
                                            style="border:0;text-align:center;"
                                            size="6"
                                            name="indo_empat"></td>
                                        <td><input
                                            type="text"
                                            style="border:0;text-align:center;"
                                            size="6"
                                            name="indo_lima"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">3</th>
                                        <td colspan="2" align="left">Bahasa Inggris</td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="ing_satu"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="ing_dua"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="ing_tiga"></td>
                                        <td><input
                                            type="text"
                                            style="border:0;text-align:center;"
                                            size="6"
                                            name="ing_empat"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="ing_lima"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">4</th>
                                        <td colspan="2" align="left">Matematika</td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="mtk_satu"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="mtk_dua"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="mtk_tiga"></td>
                                        <td><input
                                            type="text"
                                            style="border:0;text-align:center;"
                                            size="6"
                                            name="mtk_empat"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="mtk_lima"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">5</th>
                                        <td colspan="2" align="left">IPA</td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="ipa_satu"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="ipa_dua"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="ipa_tiga"></td>
                                        <td><input
                                            type="text"
                                            style="border:0;text-align:center;"
                                            size="6"
                                            name="ipa_empat"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="ipa_lima"></td>
                                    </tr>
                                    <tr>
                                        <th scope="row">6</th>
                                        <td colspan="2" align="left">IPS</td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="ips_satu"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="ips_dua"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="ips_tiga"></td>
                                        <td><input
                                            type="text"
                                            style="border:0;text-align:center;"
                                            size="6"
                                            name="ips_empat"></td>
                                        <td><input type="text" style="border:0;text-align:center;" size="6" name="ips_lima"></td>
                                    </tr>
                                    <tr>
                                        <th rowspan="2">7</th>
                                        <td colspan="2" align="left">Prestasi yang pernah diraih
                                            <br>
                                            <small>*Kosongkan jika tidak ada</small>
                                        </td>
                                        <td colspan="5"><input type="text" style="border:0;" size="100" name="prestasi_diraih"></td>
                                    </tr>
                                </tbody>
                            </table>
                            <div>
                                <button type="submit" class="btn btn-success" style="float:right">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</form>
<br>
<br>
<br>
</body>
</html>

<script>
function price() {
const tes = document
    .getElementById("tanggal")
    .value;
const tes2 = document
    .getElementById("kota")
    .value;
document
    .getElementById("tujuan")
    .value = tes2 + " - " + tes;

const tes_ayah = document
    .getElementById("tanggal_ayah")
    .value;
const tes_ayah2 = document
    .getElementById("kota_ayah")
    .value;
document
    .getElementById("tujuan_ayah")
    .value = tes_ayah2 + " - " + tes_ayah;

const tes_ibu = document
    .getElementById("tanggal_ibu")
    .value;
const tes_ibu2 = document
    .getElementById("kota_ibu")
    .value;
document
    .getElementById("tujuan_ibu")
    .value = tes_ibu2 + " - " + tes_ibu;

const tes_wali = document
    .getElementById("tanggal_wali")
    .value;
const tes_wali2 = document
    .getElementById("kota_wali")
    .value;
document
    .getElementById("tujuan_wali")
    .value = tes_wali2 + " - " + tes_wali;
}
</script>
