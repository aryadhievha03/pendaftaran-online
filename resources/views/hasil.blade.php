<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link
            rel="stylesheet"
            href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
            integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
            crossorigin="anonymous">
        <script
            src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
        <script
            src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
            integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
            crossorigin="anonymous"></script>
        <script src="https://kit.fontawesome.com/f38b57ad54.js" crossorigin="anonymous"></script>
        <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
            integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl"
            crossorigin="anonymous"></script>
        <title>Document</title>
    </head>
    <body>
        <nav class="navbar" style="background-color:#013243">
            <a class="navbar-brand" href="#" style="color:white;">Pendaftaran Siswa Online</a>
        </nav>

        <div
            class="modal fade"
            id="exampleModal"
            tabindex="-1"
            role="dialog"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title" id="exampleModalLabel">Terimakasih Telah Mendaftar</h6>
                    </div>
                    <div class="modal-body">

                        <div class="row" style="text-align: center;">

                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label id="status">Berikut berkas yang harus dibawa saat seleksi</label><br><br>
                                    <ul style="text-align:left;">
                                        <li>
                                            Fotokopi Raport SMP Kelas 7 - 9
                                        </li>
                                        <li>
                                            Fotokopi Akta Kelahiran
                                        </li>
                                        <li>
                                            Fotokopi Kartu Keluarga
                                        </li>
                                        <li>
                                            Pas Photo Berwarna(berkemeja) 3 x 4 (6 lembar)
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <a href="/">
                                <button type="button" class="btn btn-danger">Close</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                $('#exampleModal').modal('show');
            </script>
        </body>
    </html>
