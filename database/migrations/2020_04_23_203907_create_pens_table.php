<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pens', function (Blueprint $table) {
            //data calon murid
            $table->bigIncrements('id');
            $table->string('foto');
            $table->string('nama_lengkap');
            $table->string('nama_panggilan');
            $table->string('jenis_kelamin');
            $table->string('ttl');
            $table->string('agama');
            $table->string('cita_cita');
            $table->string('hobi');
            $table->string('anak_ke');
            $table->string('kandung');
            $table->string('tiri');
            $table->string('angkat');
            $table->string('berat_badan');
            $table->string('tinggi_badan');
            $table->string('gol_darah');

            //keterangan tempat tinggal
            $table->string('alamat_rumah');
            $table->string('rt');
            $table->string('rw');
            $table->string('kelurahan');
            $table->string('kecamatan');
            $table->string('kota_kabupaten');
            $table->string('provinsi');
            $table->string('kode_pos');
            $table->string('no_telp_rumah');
            $table->string('email');
            $table->string('tinggal_dengan');

            //keterangan kesehatan
            $table->string('penyakit_diderita')->nullable();
            $table->string('kelainan_lain')->nullable();

            //ayah
            $table->string('nama_ayah');
            $table->string('keberadaan_ayah');
            $table->string('ttl_ayah');
            $table->string('pekerjaan_ayah');
            $table->string('pendidikan_ayah');
            $table->string('wn_ayah');
            $table->string('agama_ayah');
            $table->string('telp_ayah');

            //ibu
            $table->string('nama_ibu');
            $table->string('keberadaan_ibu');
            $table->string('ttl_ibu');
            $table->string('pekerjaan_ibu');
            $table->string('pendidikan_ibu');
            $table->string('wn_ibu');
            $table->string('agama_ibu');
            $table->string('telp_ibu');

            //wali
            $table->string('nama_wali')->nullable();
            $table->string('ttl_wali')->nullable();
            $table->string('pekerjaan_wali')->nullable();
            $table->string('pendidikan_wali')->nullable();
            $table->string('hubungan_wali')->nullable();
            $table->string('wn_wali')->nullable();
            $table->string('agama_wali')->nullable();
            $table->string('telp_wali')->nullable();
            $table->string('email_wali')->nullable();

            //nilai rapot
            //pai
            $table->integer('pai_satu');
            $table->integer('pai_dua');
            $table->integer('pai_tiga');
            $table->integer('pai_empat');
            $table->integer('pai_lima');

            //indo
            $table->integer('indo_satu');
            $table->integer('indo_dua');
            $table->integer('indo_tiga');
            $table->integer('indo_empat');
            $table->integer('indo_lima');

            //ing
            $table->integer('ing_satu');
            $table->integer('ing_dua');
            $table->integer('ing_tiga');
            $table->integer('ing_empat');
            $table->integer('ing_lima');

            //mtk
            $table->integer('mtk_satu');
            $table->integer('mtk_dua');
            $table->integer('mtk_tiga');
            $table->integer('mtk_empat');
            $table->integer('mtk_lima');

            //ipa
            $table->integer('ipa_satu');
            $table->integer('ipa_dua');
            $table->integer('ipa_tiga');
            $table->integer('ipa_empat');
            $table->integer('ipa_lima');

            //ips
            $table->integer('ips_satu');
            $table->integer('ips_dua');
            $table->integer('ips_tiga');
            $table->integer('ips_empat');
            $table->integer('ips_lima');

            //prestasi
            $table->string('prestasi_diraih')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pens');
    }
}
