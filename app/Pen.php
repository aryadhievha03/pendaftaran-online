<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pen extends Model
{
    protected $fillable=['foto',
                        'nama_lengkap',
                        'nama_panggilan',
                        'jenis_kelamin',
                        'ttl',
                        'agama',
                        'cita_cita',
                        'hobi',
                        'anak_ke',
                        'kandung',
                        'tiri',
                        'angkat',
                        'berat_badan',
                        'tinggi_badan',
                        'gol_darah',
                        //keterangan tempat tinggal
                        'alamat_rumah',
                        'rt',
                        'rw',
                        'kelurahan',
                        'kecamatan',
                        'kota_kabupaten',
                        'provinsi',
                        'kode_pos',
                        'no_telp_rumah',
                        'email',
                        'tinggal_dengan',
                        //keterangan kesehatan
                        'penyakit_diderita',
                        'kelainan_lain',
                        //ayah
                        'nama_ayah',
                        'keberadaan_ayah',
                        'ttl_ayah',
                        'pekerjaan_ayah',
                        'pendidikan_ayah',
                        'wn_ayah',
                        'agama_ayah',
                        'telp_ayah',
                        //ibu
                        'nama_ibu',
                        'keberadaan_ibu',
                        'ttl_ibu',
                        'pekerjaan_ibu',
                        'pendidikan_ibu',
                        'wn_ibu',
                        'agama_ibu',
                        'telp_ibu',
                        //wali
                        'nama_wali',
                        'ttl_wali',
                        'pekerjaan_wali',
                        'pendidikan_wali',
                        'hubungan_wali',
                        'wn_wali',
                        'agama_wali',
                        'telp_wali',
                        'email_wali',
                        //nilai rapor
                        //pai
                        'pai_satu',
                        'pai_dua',
                        'pai_tiga',
                        'pai_empat',
                        'pai_lima',
                        //indo
                        'indo_satu',
                        'indo_dua',
                        'indo_tiga',
                        'indo_empat',
                        'indo_lima',
                        //inggris
                        'ing_satu',
                        'ing_dua',
                        'ing_tiga',
                        'ing_empat',
                        'ing_lima',
                        //mtk
                        'mtk_satu',
                        'mtk_dua',
                        'mtk_tiga',
                        'mtk_empat',
                        'mtk_lima',
                        //ipa
                        'ipa_satu',
                        'ipa_dua',
                        'ipa_tiga',
                        'ipa_empat',
                        'ipa_lima',
                        //ips
                        'ips_satu',
                        'ips_dua',
                        'ips_tiga',
                        'ips_empat',
                        'ips_lima',
                        //prestasi
                        'prestasi_diraih'
                    ];
    protected $primaryKey='id';
}
