<?php

namespace App\Http\Controllers;

use App\Pen;
use Illuminate\Http\Request;

class PenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('form');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        // $this->validate(request(),[
        //     'nama_lengkap'=>'required',
        //     'nama_panggilan'=>'required',
        //     'jenis_kelamin'=>'required|in:Laki-laki,Perempuan',
        //     'ttl'=>'required',
        //     'agama'=>'required',
        //     'cita_cita'=>'required',
        //     'hobi'=>'required',
        //     'anak_ke'=>'required',
        //     'kandung'=>'required',
        //     'tiri'=>'required',
        //     'angkat'=>'required',
        //     'berat_badan'=>'required',
        //     'tinggi_badan'=>'required',
        //     'gol_darah'=>'required',

        // ]);

        $pen= new Pen();
        $pen->nama_lengkap=$request->input('nama_lengkap');
        $pen->nama_panggilan=$request->input('nama_panggilan');
        $pen->jenis_kelamin=$request->input('jenis_kelamin');
        $pen->ttl=$request->input('ttl');
        $pen->agama=$request->input('agama');
        $pen->cita_cita=$request->input('cita_cita');
        $pen->hobi=$request->input('hobi');
        $pen->anak_ke=$request->input('anak_ke');
        $pen->kandung=$request->input('kandung');
        $pen->tiri=$request->input('tiri');
        $pen->angkat=$request->input('angkat');
        $pen->berat_badan=$request->input('berat_badan');
        $pen->tinggi_badan=$request->input('tinggi_badan');
        $pen->gol_darah=$request->input('gol_darah');

        //tempat tinggal
        $pen->alamat_rumah=$request->input('alamat_rumah');
        $pen->rt=$request->input('rt');
        $pen->rw=$request->input('rw');
        $pen->kelurahan=$request->input('kelurahan');
        $pen->kecamatan=$request->input('kecamatan');
        $pen->kota_kabupaten=$request->input('kota_kabupaten');
        $pen->provinsi=$request->input('provinsi');
        $pen->kode_pos=$request->input('kode_pos');
        $pen->no_telp_rumah=$request->input('no_telp_rumah');
        $pen->email=$request->input('email');
        $pen->tinggal_dengan=$request->input('tinggal_dengan');

        //keterangan kesehatan
        $pen->penyakit_diderita=$request->input('penyakit_diderita');
        $pen->kelainan_lain=$request->input('kelainan_lain');

        //ayah
        $pen->nama_ayah=$request->input('nama_ayah');
        $pen->keberadaan_ayah=$request->input('keberadaan_ayah');
        $pen->ttl_ayah=$request->input('ttl_ayah');
        $pen->pekerjaan_ayah=$request->input('pekerjaan_ayah');
        $pen->pendidikan_ayah=$request->input('pendidikan_ayah');
        $pen->wn_ayah=$request->input('wn_ayah');
        $pen->agama_ayah=$request->input('agama_ayah');
        $pen->telp_ayah=$request->input('telp_ayah');

        //ibu
        $pen->nama_ibu=$request->input('nama_ibu');
        $pen->keberadaan_ibu=$request->input('keberadaan_ibu');
        $pen->ttl_ibu=$request->input('ttl_ibu');
        $pen->pekerjaan_ibu=$request->input('pekerjaan_ibu');
        $pen->pendidikan_ibu=$request->input('pendidikan_ibu');
        $pen->wn_ibu=$request->input('wn_ibu');
        $pen->agama_ibu=$request->input('agama_ibu');
        $pen->telp_ibu=$request->input('telp_ibu');

        //wali
        $pen->nama_wali=$request->input('nama_wali');
        $pen->ttl_wali=$request->input('ttl_wali');
        $pen->pekerjaan_wali=$request->input('pekerjaan_wali');
        $pen->pendidikan_wali=$request->input('pendidikan_wali');
        $pen->hubungan_wali=$request->input('hubungan_wali');
        $pen->wn_wali=$request->input('wn_wali');
        $pen->agama_wali=$request->input('agama_wali');
        $pen->telp_wali=$request->input('telp_wali');
        $pen->email_wali=$request->input('email_wali');

        //nilai rapor
        //pai
        $pen->pai_satu=$request->input('pai_satu');
        $pen->pai_dua=$request->input('pai_dua');
        $pen->pai_tiga=$request->input('pai_tiga');
        $pen->pai_empat=$request->input('pai_empat');
        $pen->pai_lima=$request->input('pai_lima');

        //indo
        $pen->indo_satu=$request->input('indo_satu');
        $pen->indo_dua=$request->input('indo_dua');
        $pen->indo_tiga=$request->input('indo_tiga');
        $pen->indo_empat=$request->input('indo_empat');
        $pen->indo_lima=$request->input('indo_lima');

        //inggris
        $pen->ing_satu=$request->input('ing_satu');
        $pen->ing_dua=$request->input('ing_dua');
        $pen->ing_tiga=$request->input('ing_tiga');
        $pen->ing_empat=$request->input('ing_empat');
        $pen->ing_lima=$request->input('ing_lima');

        //mtk
        $pen->mtk_satu=$request->input('mtk_satu');
        $pen->mtk_dua=$request->input('mtk_dua');
        $pen->mtk_tiga=$request->input('mtk_tiga');
        $pen->mtk_empat=$request->input('mtk_empat');
        $pen->mtk_lima=$request->input('mtk_lima');

        //ipa
        $pen->ipa_satu=$request->input('ipa_satu');
        $pen->ipa_dua=$request->input('ipa_dua');
        $pen->ipa_tiga=$request->input('ipa_tiga');
        $pen->ipa_empat=$request->input('ipa_empat');
        $pen->ipa_lima=$request->input('ipa_lima');

        //ips
        $pen->ips_satu=$request->input('ips_satu');
        $pen->ips_dua=$request->input('ips_dua');
        $pen->ips_tiga=$request->input('ips_tiga');
        $pen->ips_empat=$request->input('ips_empat');
        $pen->ips_lima=$request->input('ips_lima');

        //prestasi
        $pen->prestasi_diraih=$request->input('prestasi_diraih');

        if($request->file('foto')){
            $file = $request->file('foto')->store('fotos', 'public');
            $pen->foto = $file;
        }
        $pen->save();
        return view('hasil');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pen  $pen
     * @return \Illuminate\Http\Response
     */
    public function show(Pen $pen)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pen  $pen
     * @return \Illuminate\Http\Response
     */
    public function edit(Pen $pen)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pen  $pen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pen $pen)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pen  $pen
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pen $pen)
    {
        //
    }
}
